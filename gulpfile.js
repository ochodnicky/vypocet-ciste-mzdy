const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const zip = require('gulp-zip');

// File paths
const SCSS_PATH = './scss/**/*.scss';
const CSS_NAME = 'main.min.css';
const HTML_PATH = './*.html';
const CSS_PATH = './css';
const SCRIPTS_PATH = './js';

// compile scss into css
function styles() {
  return gulp.src(SCSS_PATH)
    .pipe(sass({
			outputStyle: 'compressed'
	}))
	.pipe(autoprefixer())
    .pipe(concat(CSS_NAME))
    .pipe(gulp.dest(CSS_PATH))
}

// Zipping files
function exportZip() {
	return gulp.src('**/*')
		.pipe(zip('website.zip'))
		.pipe(gulp.dest('./'))
}

function watch() {
  browserSync.init({
    server: {
      baseDir: './'
    }
  });
  styles();
  gulp.watch(SCSS_PATH, styles);
  gulp.watch(HTML_PATH).on('change', browserSync.reload);
  gulp.watch(CSS_PATH).on('change', browserSync.reload);
  gulp.watch(SCRIPTS_PATH).on('change', browserSync.reload);
}

exports.styles = styles;
exports.exportZip = exportZip;
exports.watch = watch;
