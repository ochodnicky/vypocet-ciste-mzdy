'use strict';

var zamestnavatelSocPERC = 0.248,
    zamestnavatelZdravPERC = 0.09,
    zamestnanecSocPERC = 0.065,
    zamestnanecZdravPERC = 0.045,
    minimalniMzda = 14600,
    dan = 0.15,
    slevaNaDani,
    slevaNaDaniStudent = 2405,
    slevaNaDaniZaklad = 2070,
    slevaDiteJedno = 1267,
    slevaDiteDruhe = 1617,
    slevaDiteDalsi = 2017,
    slevaInvalida12 = 210,
    slevaInvalida3 = 420,
    slevaInvalidaZTP = 1345,
    slevaDetiMax = 5025,
    pocetDeti,
    pocetDeti,
    slevaDeti,
    celkovaDan,
    danBonus,
    odvodyStatu,
    zdravZamestnavatel,
    socZamestnavatel,
    zdravZamestnanec,
    socZamestnanec,
    superHruba,
    danBezSlev,
    superHrubaUp,
    hrubaMzda,
    cistaMzda;

var selHruba = document.querySelector('#hrubaMzda'),
    selPocetDeti = document.querySelector('#pocetDeti'),
    selStudent = document.querySelector('#student'),
    selManzelka = document.querySelector('#manzelka'),
    selInvalida12 = document.querySelector('#level12'),
    selInvalida3 = document.querySelector('#level3'),
    selInvalidaZTP = document.querySelector('#ztp'),
    selForm = document.querySelector('#calculateForm'),
    selSuper = document.querySelector('#superHruba'),
    selDanPred = document.querySelector('#danPred'),
    selDanBonusWrap = document.querySelector('#danBonusWrap'),
    selDanBonus = document.querySelector('#danBonus'),
    selSlevaDan = document.querySelector('#slevaDan'),
    selDan = document.querySelector('#dan'),
    selDanDeti = document.querySelector('#danDeti'),
    selDanDetiWrap = document.querySelector('#danDetiWrap'),
    selSocZamestnanec = document.querySelector('#socZamestnanec'),
    selZdravZamestnanec = document.querySelector('#zdravZamestnanec'),
    selSocZamestnavatel = document.querySelector('#socZamestnavatel'),
    selZdravZamestnavatel = document.querySelector('#zdravZamestnavatel'),
    selOdvodyStatu = document.querySelector('#odvodyStatu'),
    selCistaMzda = document.querySelector('#cistaMzda'),
    selZaklad = document.querySelector('#zaklad'),
    selResult = document.querySelector('.calculator-result'),
    selTotal = document.querySelector('.calculator-highlight'),
    selInput = document.querySelectorAll('.calculator-form__input'),
    selNumberInput = document.querySelectorAll('.number__input'),
    selNumberControl = document.querySelectorAll('.number__control'),
    selCheckboxes = document.querySelectorAll('.calculator-form .form-checkbox label');

function formatNumber(number) {
    number = Math.round(number);
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}   

function numberOnly(e) {
    var element = e.target;
    var regex = /[^0-9]/gi;
    element.value = element.value.replace(regex, "");
 }

function checkboxOne(e) {
    var checkboxSelector = e.target.closest('.form-checkbox').querySelector('input');
    var checkboxState = checkboxSelector.checked;
    var checkboxGroup = checkboxSelector.getAttribute('data-group');
    var checkboxes = document.querySelectorAll('[data-group='+ checkboxGroup +']');

    for (var i = 0; i < checkboxes.length; i++) {
        var checkbox = checkboxes[i].closest('.form-checkbox').querySelector('input');
        checkbox.checked = false;
    }

    if(checkboxState) {
        checkboxSelector.checked = true;
    } else {
        checkboxSelector.checked = false;
    }
}

function numberUpdate(e) {
    var numberType = e.target.getAttribute('data-type'),
        numberInput = e.target.closest('.number').querySelector('.number__input'),
        numberValue = numberInput.value;
        
    if (numberType == "plus") {
        if (numberValue == 10) {
            numberInput.value = 10;
        } else {
            numberInput.value = ++numberValue;
        }
    } else {
        if (numberValue <= 0) {
            numberInput.value = 0;
        } else {
            numberInput.value = --numberValue;
        }
    }
}  

function calcSuper(hruba) {
    socZamestnavatel = hruba * zamestnavatelSocPERC;
    zdravZamestnavatel = hruba * zamestnavatelZdravPERC;

    return hruba * (1 + zamestnavatelSocPERC + zamestnavatelZdravPERC);
}

function calcCista(hrubaMzda, superHrubaUp) {
    danBezSlev = superHrubaUp * dan;
    celkovaDan = danBezSlev - slevaNaDani;
    if(celkovaDan < 0) {
        celkovaDan =  0;
    }
    if(hrubaMzda < minimalniMzda) {
        zdravZamestnanec = (minimalniMzda * (zamestnanecZdravPERC + zamestnavatelZdravPERC)) - zdravZamestnavatel;
        console.log("Zdrav min: " + zdravZamestnanec);
    } else {
        zdravZamestnanec = hrubaMzda * zamestnanecZdravPERC;
    }
    socZamestnanec = hrubaMzda * zamestnanecSocPERC;

    if(slevaDeti > 0) {
        danBonus =  slevaDeti - celkovaDan;
    }
    //maximální daňový bonus
    if(danBonus > slevaDetiMax) {
        danBonus = slevaDetiMax;
    }
    //maximální sleva na děti
    if( slevaDeti > (celkovaDan + danBonus)) {
        slevaDeti = celkovaDan + danBonus;
    }

    return hrubaMzda - zdravZamestnanec - socZamestnanec - celkovaDan + slevaDeti;
}

function calcSlevaDite(pocetDeti) {
    var slevaDeti;
    if (pocetDeti == 1) {
        slevaDeti = slevaDiteJedno;
    } else if (pocetDeti == 2) {
        slevaDeti = slevaDiteJedno + slevaDiteDruhe;
    } else if (pocetDeti >= 3) {
        slevaDeti = slevaDiteJedno + slevaDiteDruhe + ((pocetDeti - 2) * slevaDiteDalsi);
    } else {
        slevaDeti = 0;
    }
    console.log('Sleva na děti: ' + slevaDeti);
    return slevaDeti;
}

function calculate(e) {
    e.preventDefault();

    hrubaMzda = selHruba.value;
    
    if(hrubaMzda) {
        slevaNaDani = slevaNaDaniStudent;
    }

    var isStudent = selStudent.checked,
        isWife = selManzelka.checked,
        isInvalida12 = selInvalida12.checked,
        isInvalida3 = selInvalida3.checked,
        isInvalidaZTP = selInvalidaZTP.checked,
        pocetDeti = selPocetDeti.value;

    if(isStudent) {
        slevaNaDani = slevaNaDaniStudent;
    } else if (isWife) {
        slevaNaDani = slevaNaDaniZaklad * 2; 
    } else {
        slevaNaDani = slevaNaDaniZaklad;
    }

    if(isInvalida12) {
        slevaNaDani += slevaInvalida12;
    } else if (isInvalida3) {
        slevaNaDani += slevaInvalida3; 
    } else if (isInvalidaZTP) {
        slevaNaDani += slevaInvalidaZTP; 
    } else {
        slevaNaDani = slevaNaDani;
    }

    slevaDeti = calcSlevaDite(pocetDeti);
    superHruba = calcSuper(hrubaMzda);
    superHrubaUp = Math.ceil(superHruba / 100.0) * 100;
    cistaMzda = calcCista(hrubaMzda, superHrubaUp);
    odvodyStatu = socZamestnanec + zdravZamestnanec + socZamestnavatel + zdravZamestnavatel + (celkovaDan - slevaDeti);

    // render to DOM
    document.querySelector('#superHruba').innerHTML = formatNumber(superHruba);
    document.querySelector('#danPred').innerHTML = formatNumber(danBezSlev);
    document.querySelector('#slevaDan').innerHTML = formatNumber(slevaNaDani);
    document.querySelector('#dan').innerHTML = formatNumber(celkovaDan);
    document.querySelector('#danDeti').innerHTML = formatNumber(slevaDeti);
    document.querySelector('#danBonus').innerHTML = formatNumber(danBonus);
    document.querySelector('#socZamestnanec').innerHTML = formatNumber(socZamestnanec);
    document.querySelector('#zdravZamestnanec').innerHTML = formatNumber(zdravZamestnanec);
    document.querySelector('#socZamestnavatel').innerHTML = formatNumber(socZamestnavatel);
    document.querySelector('#zdravZamestnavatel').innerHTML = formatNumber(zdravZamestnavatel);
    document.querySelector('#odvodyStatu').innerHTML = formatNumber(odvodyStatu);
    document.querySelector('#zaklad').innerHTML = formatNumber(superHrubaUp);
    document.querySelector('#cistaMzda').innerHTML = formatNumber(cistaMzda);

    if (danBonus > 0) {
        selDanBonusWrap.classList.remove('hidden');
    } else {
        selDanBonusWrap.classList.add('hidden');
    }
    if (pocetDeti > 0) {
        selDanDetiWrap.classList.remove('hidden');
    } else {
        selDanDetiWrap.classList.add('hidden');
    }
    selResult.classList.add('open');
    selTotal.classList.remove('hidden');
}


// Event listeners
selForm.addEventListener('submit', calculate);

for (var i = 0; i < selNumberControl.length; i++) {
    selNumberControl[i].addEventListener('click', numberUpdate);   
}
for (var i = 0; i < selNumberInput.length; i++) {
    selNumberInput[i].addEventListener('input', numberOnly);
}
for (var i = 0; i < selInput.length; i++) {
    selInput[i].addEventListener('input', numberOnly);
}
for (var i = 0; i < selCheckboxes.length; i++) {
    selCheckboxes[i].addEventListener('click', checkboxOne);
}

var selMenuToggler = document.querySelector('.header-menu'),
    selMenuWrapper = document.querySelector('.menu-wrapper'),
    selOverlay = document.querySelector('.overlay'),
    selMenuClose = document.querySelector('.menu-close');

function toggleMenu() {
    selOverlay.classList.toggle('open');
    selMenuToggler.classList.toggle('open');
    selMenuWrapper.classList.toggle('open');
}

selMenuToggler.addEventListener('click', toggleMenu);
selMenuClose.addEventListener('click', toggleMenu);
selOverlay.addEventListener('click', toggleMenu);